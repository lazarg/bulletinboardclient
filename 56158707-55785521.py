from socket import *

# Functions used throughout this client program:
# Post messages if command is "POST"


def postCmd(payload):
    # Initialize line to an empty string so that we can enter the loop
    line = ""

    # We need to read lines until "."
    # Append lines to payload until we enter "."
    while (line != "."):
        line = input(" > ")
        payload += line + "\n"
        print("client:", line)

    # Send the encoded payload
    clientSocket.send(payload.encode())

    # Accept and decode the response from server
    response = clientSocket.recv(1024).decode()
    print("server:", response)

    return response


# Read message sent from server if command is "READ"
def readCmd(payload):
    # Send the encoded payload
    clientSocket.send(payload.encode())

    # Accept and decode the first response from server
    response = clientSocket.recv(1024).decode()

    # For some reason, when running READ, server sent the last dot with a \0 after (".\0")
    # Thus, output sanitation is required.
    while (response.replace("\0", "") != "."):
        print("server:", response)
        response = clientSocket.recv(1024).decode()

    # Print the dot to signal end of server's message
    print("server:", response)

    return response


def cmd(payload):
    # Send the encoded payload
    clientSocket.send(payload.encode())

    # Accept and decode the response from server
    response = clientSocket.recv(1024).decode()
    print("server:", response)

    return response


# Initialize socket and print the IP and port number
clientSocket = socket(AF_INET, SOCK_STREAM)
ipAddress = input("Input the IP address:\n > ")
portNumber = 16000

print("IP address:", ipAddress, "\tPort number:", portNumber)

# Try-except handles errors (most likely with establishing connection
# but it should be foolproof with a try-except around the main functions)
try:
    # Connect to the IP and port number
    clientSocket.connect((ipAddress, portNumber))

    # If it doesn't crash, it will print this
    # If there's an error, it will fall back to the except
    print("Connection status: Success!")

    # To check when the program should stop
    ended = False

    while (not ended):

        # Enter the command
        command = input(" > ")

        # Print the command
        print("client:", command)

        # Save this to payload so that we can add more if the command is POST
        # and to keep it in a format expected by server
        payload = command + "\n"

        # If the command is post, we need to read lines until "."
        if (command == "POST"):
            postCmd(payload)

        # If the command is "READ" we should expect server responses until we get "."
        elif (command == "READ"):
            readCmd(payload)

        # If the command is "QUIT" and the response was "OK", the program has ended
        elif (command == "QUIT"):
            if (cmd(payload) == "OK"):
                ended = True

        # If the command is not all of the above, send the payload containing
        # only the command, then continue
        else:
            cmd(payload)
            continue

    # Close the socket
    clientSocket.close()

# Exception: connection error
except Exception as exc:

    # Print error info
    print("Connection status: Error!")
    print(exc)

    # Close the socket even when an error occurs (might be open)
    clientSocket.close()
